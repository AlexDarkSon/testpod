Pod::Spec.new do |s|
  s.name         = "TestPod"
  s.version      = "0.0.1"
  s.summary      = "Summary"
  s.homepage     = "https://bitbucket.org/AlexDarkSon/TestPod.git"
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author             = { "Evtuhov" =>"test@test.ru"}
  s.platform     = :ios
  s.platform     = :ios, "10.0"
  s.source       = { :git => "https://bitbucket.org/AlexDarkSon/TestPod.git", :tag => "#{s.version}" }

  s.source_files  = "Classes", "Classes/**/*.{h,m,swift}"
  s.exclude_files = "Classes/Exclude"
  s.framework  = "Foundation"

  s.requires_arc = true


end
